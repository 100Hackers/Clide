let Http = {
    get(url) {

    },
    post(url, data, callback = null, error = null) {
        $.ajax(url, {
            data: JSON.stringify(data),
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },        
            error: error,
            method: 'POST',
            success: callback
        });
    }
};