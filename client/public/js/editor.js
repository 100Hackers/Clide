let Editor = {
    breakpoints: [],
    instance: null,
    getText() {
        if (this.instance) {
            let session = this.instance.getSession();
            if (session) {
                return session.getValue();
            }
        }
        return '';
    },
    init(ace) {
        this.instance = ace.edit('editor');
        this.instance.setTheme('ace/theme/monokai');
        this.instance.session.setMode('ace/mode/javascript');

        this.instance.renderer.setShowGutter(true);
        this.instance.on('guttermousedown', e => {
            let target = e.domEvent.target;

            if (target.className.indexOf('ace_gutter-cell') == -1) {
                return;
            }
            if (!this.instance.isFocused()) {
                return;
            }
            if (e.clientX > 25 + target.getBoundingClientRect().left) {
                return;
            }
            let row = e.getDocumentPosition().row;
            this.breakpoints = e.editor.session.getBreakpoints();
            
            if (!(row in this.breakpoints)) {
                e.editor.session.setBreakpoint(row);
            } else {
                e.editor.session.clearBreakpoint(row);
            }
            e.stop();
        });
        this.instance.session.on('changeBreakpoint', e => {
            console.log(Editor.breakpoints);
        });
    },
    runCode() {
        let files = [this.getText()];
        let language = $('#languageSelect option:selected').val();
        console.log(language);
        Http.post('http://localhost:5000/api/Compile', {
            files: files,
            language: language
        }, response => {
            console.log(response);
            $('#result').html(response.output);
            $('#error').html(response.error);
        }, error => {
            console.error(error);
        });
    }
};

$(document).ready(() => {
    Editor.init(ace);
});