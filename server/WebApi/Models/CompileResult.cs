#pragma warning disable 1591

using System;
using WebApi.Enums;

namespace WebApi.Models
{
    public class CompileResult
    {
        public ExecutionStatus ExecutionStatus;
        public int? ExitCode;
        public string Error;
        public string Output;
    }
}

#pragma warning restore 1591