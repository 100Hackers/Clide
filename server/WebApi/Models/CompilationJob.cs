#pragma warning disable 1591

using System;
using System.Collections.Generic;
using WebApi.Enums;

namespace WebApi.Models
{
    public class CompilationJob
    {
        public List<string> Files;
        public Language Language;
    }
}

#pragma warning restore 1591