﻿#pragma warning disable 1591

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Mvc.Cors.Internal;

namespace WebApi
{
    public class Startup
    {
        private const string CORS_POLICY_NAME = "CorsPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            });

            services.AddMvc(options => {
                options.Filters.Add(new CorsAuthorizationFilterFactory(CORS_POLICY_NAME));
            });

            CreateAndRegisterCorsPolicy(services, new CorsPolicyBuilder(), "http://localhost:3000");

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { 
                    Title = "Clide",
                    Version = "v1" 
                });
                
                var filePath = Path.Combine(AppContext.BaseDirectory, "WebApi.xml");
                c.IncludeXmlComments(filePath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Topick V1");
            });

            app.UseMvc();
        }

        private void CreateAndRegisterCorsPolicy(IServiceCollection services, CorsPolicyBuilder corsBuilder, string origin = null)
        {
            CreateCorsPolicy(corsBuilder, origin);
            RegisterCorsPolicy(services, corsBuilder);
        }

        private void CreateCorsPolicy(CorsPolicyBuilder corsBuilder, string origin = null)
        {
            corsBuilder.AllowAnyHeader()
                       .AllowAnyMethod()
                       .AllowCredentials();

            if (origin == null)
            {
                corsBuilder.AllowAnyOrigin();
            }
            else
            {
                corsBuilder.WithOrigins(origin);
            }
        }

        private void RegisterCorsPolicy(IServiceCollection services, CorsPolicyBuilder corsBuilder)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(CORS_POLICY_NAME, corsBuilder.Build());
            });
        }
    }
}

#pragma warning restore 1591