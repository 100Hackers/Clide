using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Enums;

namespace WebApi.Controllers
{
    #pragma warning disable 1591
    [Route("api/[controller]")]
    [ApiController]
    public class CompileController : ControllerBase
    {
        #pragma warning restore 1591

        /// <summary>
        /// Submits code to execute
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Compile
        ///     {
        ///         "files": [
        ///             "console.log('Hello World'); console.error('Goodbye World');"
        ///         ],
        ///         "language": "Javascript"
        ///     }
        ///
        /// </remarks>
        /// <returns>Test values</returns>
        /// <response code="200">Program finished execution</response>
        /// <response code="500">Program timed out</response>
        [HttpPost]
        public ActionResult<CompileResult> Compile(CompilationJob compilationJob)
        {
            try
            {
                var compileResult = new CompileResult();
                if (compilationJob != null)
                {
                    switch(compilationJob.Language)
                    {
                        case Language.CPlusPlus:
                            compileResult = Compile(compilationJob, "cppBash", ".cpp");
                            break;
                        case Language.CSharp:
                            compileResult = Compile(compilationJob, "cSharpBash", ".cs");
                            break;
                        case Language.Java:
                            compileResult = Compile(compilationJob, "javaBash", ".java");
                            break;
                        case Language.Javascript:
                            compileResult = Compile(compilationJob, "javascriptBash", ".js");
                            break;
                        default:
                            return BadRequest("Invalid language provided");
                    }
                }
                return new OkObjectResult(compileResult);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #pragma warning disable 1591
        private CompileResult Compile(CompilationJob compilationJob, string runScript, string pathSuffix = "")
        {
            var compileResult = new CompileResult();
            var process = new Process();
            var path = System.IO.Path.GetTempFileName();
            path += pathSuffix;
            
            using (var fileWriter = System.IO.File.CreateText(path))
            {
                foreach (var file in compilationJob.Files)
                {
                    fileWriter.Write(file);
                }
            }

            process.StartInfo.FileName = $"RunScripts/{runScript}";
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.Arguments = path;
            process.Start();
            process.WaitForExit();
            
            compileResult.Output = process.StandardOutput.ReadToEnd();
            compileResult.Error = process.StandardError.ReadToEnd();
            compileResult.ExitCode = process.ExitCode;
            compileResult.ExecutionStatus = ExecutionStatus.Success;
            
            System.IO.File.Delete(path);
            
            return compileResult;
        }
        #pragma warning restore 1591
    }
}
