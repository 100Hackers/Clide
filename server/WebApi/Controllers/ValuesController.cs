﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    #pragma warning disable 1591
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        #pragma warning restore 1591

        /// <summary>
        /// Returns test values
        /// </summary>
        /// <returns>Test values</returns>
        /// <response code="200">Returns test values</response>
        /// <response code="500">Something failed on the serializer</response>
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Returns the id sent to it
        /// </summary>
        /// <returns>Id</returns>
        /// <response code="200">Returns id</response>
        /// <response code="500">Something failed on the serializer</response>
        [HttpGet("{id}")]
        public ActionResult<int> Get(int id)
        {
            return id;
        }
    }
}
