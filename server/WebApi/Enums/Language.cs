#pragma warning disable 1591

using System;

namespace WebApi.Enums
{
    public enum Language
    {
        CPlusPlus,
        CSharp,
        Java,
        Javascript
    }
}

#pragma warning restore 1591