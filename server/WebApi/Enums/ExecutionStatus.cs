#pragma warning disable 1591

using System;

namespace WebApi.Enums
{
    public enum ExecutionStatus
    {
        Malicious,
        NotImplemented,
        None,
        Success,
        TimeOut
    }
}

#pragma warning restore 1591