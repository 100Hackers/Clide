using Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Data
{
    public class TemporaryDbContextFactory : IDesignTimeDbContextFactory<ClideContext>
    {
        ClideContext IDesignTimeDbContextFactory<ClideContext>.CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ClideContext>();
            builder.UseSqlServer("Server=localhost;Database=master;User=sa;Password=Thisisatestdatabase1;");
            return new ClideContext(builder.Options);
        }
    }
}
